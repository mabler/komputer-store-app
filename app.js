let currentPay = document.getElementById("salary-amount");
let bankBalance = document.getElementById("balance");
let outstandingLoan = document.getElementById("outstanding-amount");
let repayBtn = document.getElementById("repay-btn");
let laptopsElement = document.getElementById("laptop-select");
let featuresElement = document.getElementById("features-container");
let imageElement = document.getElementById("img-container");
let laptopInfoElement = document.getElementById("laptop-info-container");
let buyInfoElement = document.getElementById("buy-info-container");
let buyBtn = document.getElementById("buy-btn");
let laptopImg = document.getElementById("laptop-img");
let laptopTitle = document.getElementById("laptop-title");
let laptopDesc = document.getElementById("laptop-desc");
let laptopPrice = document.getElementById("laptop-price");
let featureElement = document.getElementById("features");
let outstandingLoanAmount = "";
let laptopData = "";
let currentLaptopId = 0

function increasePay() {
  let parsedPay = parseInt(currentPay.innerHTML);
  currentPay.innerHTML = parsedPay + 100;
}

function transferSalary() {
  let loanAmount = parseInt(outstandingLoanAmount.innerHTML);
  let payAmount = parseInt(currentPay.innerHTML);
  let bankAmount = parseInt(bankBalance.innerHTML);

  if (loanAmount > 0) {
    // Amount to pay off loan
    let amountToPayOff = payAmount * 0.1;

    if (amountToPayOff > loanAmount) {
      // Calculate the remainder of pay after the loan is paid off.
      let remainder = (amountToPayOff -= loanAmount);

      outstandingLoan.innerHTML = 0;

      // Add the remainder of the pay to the bank
      bankBalance.innerHTML = bankAmount + remainder;
      currentPay.innerHTML = 0;

      outstandingLoan.setAttribute("hidden", "");
      repayBtn.setAttribute("hidden", "");
    }

    // Reduce the outstanding loan amount
    loanAmount -= amountToPayOff;
    outstandingLoanAmount.innerHTML = loanAmount;

    // Reduce the pay with the amount paid off.
    payAmount *= 0.9;

    // Add pay to bank account
    bankBalance.innerHTML = bankAmount + payAmount;
    currentPay.innerHTML = 0;
  } else {
    bankBalance.innerHTML = bankAmount + payAmount;
    currentPay.innerHTML = 0;
  }
}

function getLoan() {
  parsedBankBalance = parseInt(bankBalance.innerHTML);
  console.log(outstandingLoanAmount.innerHTML);

  if (parseInt(outstandingLoanAmount.innerHTML) > 0) {
    alert("Cannot have more than one outstanding loan.");
  } else {
    let amountToLoan = prompt("Enter amount to loan");
    amountToLoan = parseInt(amountToLoan);

    if (amountToLoan) {
      if (amountToLoan > parsedBankBalance * 2) {
        console.log("too much");
      } else {
        bankBalance.innerHTML = parsedBankBalance + amountToLoan;
        outstandingLoan.innerHTML = `Outstanding amount <span id="outstanding-loan-span">${amountToLoan} </span>kr`;

        outstandingLoan.removeAttribute("hidden");
        repayBtn.removeAttribute("hidden");
      }
    }
  }

  outstandingLoanAmount = document.getElementById("outstanding-loan-span");
  console.log(outstandingLoanAmount.innerHTML);
}

// function for repaying loan
function repayLoan() {
  let loanAmount = parseInt(outstandingLoanAmount.innerHTML);
  let payAmount = parseInt(currentPay.innerHTML);
  let bankAmount = parseInt(bankBalance.innerHTML);

  
  if (payAmount > loanAmount) {
    let remainder = (payAmount -= loanAmount);
    outstandingLoanAmount.innerHTML = "";
    bankBalance.innerHTML = bankAmount + remainder;
    currentPay.innerHTML = 0;

    outstandingLoan.setAttribute("hidden", "");
    repayBtn.setAttribute("hidden", "");
  } else {
    // Reduce outstanding loan with pay amount.
    outstandingLoanAmount.innerHTML = loanAmount - payAmount;
    currentPay.innerHTML = 0;
  }
}

// Get laptopdata from API
(async function () {
  let response = await fetch(
    "https://noroff-komputer-store-api.herokuapp.com/computers"
  );
  laptopData = await response.json();

  addLaptops(laptopData);
  showLaptopInfo(laptopData[0]);
  populateFeatures(laptopData[0]);
})();

// Add laptops to selection options
function addLaptops(laptops) {
  for (let laptop of laptops) {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
  }
}

// Function for handling the change of options.
// I could not get it to work with a ordinary function declaration, so I followed the example.
const handleLaptopChange = (e) => {
  removeChildren(featuresElement);

  const selectedLaptop = laptopData[e.target.selectedIndex];
  populateFeatures(selectedLaptop);
  showLaptopInfo(selectedLaptop);
};

// Populate features section
function populateFeatures(laptop) {
  featureElement.innerHTML = "";

  // For each spec, write out spec text.
  for (spec of laptop.specs) {
    let featureElement = document.createElement("p");
    featureElement.appendChild(document.createTextNode(spec));
    featuresElement.appendChild(featureElement);
  }
}

// Function for populating the laptop info section
function showLaptopInfo(laptop) {
  // Remove the info already present
  removeChildren(laptopInfoElement);
  removeChildren(imageElement);
  removeChildren(buyInfoElement);

  // Create all the elements and add content
  const laptopImg = document.createElement("img");
  const laptopTitle = document.createElement("h1");
  const laptopDesc = document.createElement("p");
  const laptopPrice = document.createElement("h3");
  const buyBtn = document.createElement("button");

  currentLaptopId = laptop.id

  laptop.id == 5
    ? (laptopImg.src =
        "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png")
    : (laptopImg.src = `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`);
  laptopImg.alt = "Picture of a computer";
  imageElement.appendChild(laptopImg);

  laptopTitle.appendChild(document.createTextNode(laptop.title));
  laptopInfoElement.appendChild(laptopTitle);

  laptopDesc.appendChild(document.createTextNode(laptop.description));
  laptopInfoElement.appendChild(laptopDesc);

  laptopPrice.appendChild(document.createTextNode(laptop.price + " kr"));
  buyInfoElement.appendChild(laptopPrice);

  buyBtn.id = "buy-btn";
  buyBtn.onclick = buyLaptop;
  buyBtn.appendChild(document.createTextNode("Buy now"));
  buyInfoElement.appendChild(buyBtn);
}

// Function for removing the child elements
function removeChildren(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}
// Function to buy that laptop
function buyLaptop() {
  let selectedLaptop =  laptopData[currentLaptopId -1]

  if (parseInt(bankBalance.innerHTML) >= selectedLaptop.price) {
    alert(`You are now the owner of a ${selectedLaptop.title}!`);
    bankBalance.innerHTML = bankBalance.innerHTML -= selectedLaptop.price;
  } else {
    alert("Not enough money");
  }
}

laptopsElement.addEventListener("change", handleLaptopChange);
